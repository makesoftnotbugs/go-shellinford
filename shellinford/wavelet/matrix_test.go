// Copyright 2013 The Shellinford-Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package wavelet

import (
	"bitbucket.org/oov/go-shellinford/shellinford/bit"
	"bytes"
	"testing"
)

func setup() (str []byte, rd, sd, td [][]int, wm *Matrix) {
	str = []byte("abracadabra mississippi")

	wm = NewMatrix(str)

	rd = make([][]int, 256)
	sd = make([][]int, 256)
	td = make([][]int, 256)
	for c := 0; c < 256; c++ {
		rd[c] = append(rd[c], 0)
		td[c] = append(td[c], 0)
	}

	for i, stri := range str {
		for c := 0; c < 256; c++ {
			rd[c] = append(rd[c], rd[c][i])
			td[c] = append(td[c], td[c][i])
			if int(stri) == c {
				rd[c][i+1]++
				sd[c] = append(sd[c], i)
			}
			if int(stri) < c {
				td[c][i+1]++
			}
		}
	}
	return
}

func BenchmarkGet(b *testing.B) {
	_, _, _, _, wm := setup()
	for n := 0; n < b.N; n++ {
		for i := 0; i < wm.Len(); i++ {
			wm.Get(i)
		}
	}
}

func BenchmarkRank(b *testing.B) {
	_, _, _, _, wm := setup()
	for n := 0; n < b.N; n++ {
		for c := 0; c < 256; c++ {
			for i := 0; i <= wm.Len(); i++ {
				wm.Rank(i, byte(c))
			}
		}
	}
}

func TestBuild(t *testing.T) {
	wm := NewMatrix([]byte{
		11, 0, 15, 6, 5, 2, 7, 12, 11, 0, 12, 12, 13, 4, 6, 13, 1, 11, 6, 1, 7, 10, 2, 7, 14, 11, 1, 7, 5, 4, 14, 6,
	})
	success := [][]int{
		[]int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		[]int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		[]int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		[]int{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
		[]int{1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 0},
		[]int{0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1},
		[]int{0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1},
		[]int{0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0},
	}
	for depth, ints := range success {
		for i, v := range ints {
			f := v != 0
			if wm.d[depth].bv.Get(i) != f {
				t.Fail()
			}
		}
	}
	wm.Rank(22, 11)
}

func TestLen(t *testing.T) {
	str, rd, _, _, wm := setup()
	if len(str) != wm.Len() {
		t.Logf("wm.Len() = %d / Success = %d", wm.Len(), len(str))
		t.Fail()
	}
	for c := 0; c < 256; c++ {
		if wm.RankLen(byte(c)) != rd[c][wm.Len()] {
			t.Logf("wm.RankLen(%d) = %d / Success = %d", c, wm.RankLen(byte(c)), rd[c][wm.Len()])
			t.Fail()
		}
	}
}

func TestGet(t *testing.T) {
	str, _, _, _, wm := setup()
	for i := 0; i < wm.Len(); i++ {
		if wm.Get(i) != str[i] {
			t.Logf("wm.Get(%d) = %d / Success = %d / %b != %b", i, wm.Get(i), str[i], wm.Get(i), str[i])
			t.Fail()
		}
	}
}

func TestRank(t *testing.T) {
	_, rd, _, _, wm := setup()
	for c := 0; c < 256; c++ {
		for i := 0; i <= wm.Len(); i++ {
			if wm.Rank(i, byte(c)) != rd[c][i] {
				t.Logf("wm.Rank(%d, %d) = %d / Success = %d", i, c, wm.Rank(i, byte(c)), rd[c][i])
				t.Fail()
			}
		}
	}
}

func TestSelect(t *testing.T) {
	_, _, sd, _, wm := setup()
	for c := 0; c < 256; c++ {
		for i := 0; i < wm.RankLen(byte(c)); i++ {
			if wm.Select(i, byte(c)) != sd[c][i] {
				t.Logf("wm.Select(%d, %d) = %d / Success = %d", i, c, wm.Select(i, byte(c)), sd[c][i])
				t.Fail()
			}
		}
	}
}

func TestRankLessThan(t *testing.T) {
	_, _, _, td, wm := setup()
	for c := 0; c < 256; c++ {
		for i := 0; i <= wm.Len(); i++ {
			if wm.RankLessThan(i, byte(c)) != td[c][i] {
				t.Logf("wm.RankLessThan(%d, %d) = %d / Success = %d", i, c, wm.RankLessThan(i, byte(c)), td[c][i])
				t.Fail()
			}
		}
	}
}

func TestGetBoundary(t *testing.T) {
	t.Skip("bounds checking is disabled")
	str, _, _, _, wm := setup()
	defer func() {
		if err := recover(); err == nil {
			t.Fail()
		}
	}()
	wm.Get(len(str))
}

func TestRankBoundary(t *testing.T) {
	t.Skip("bounds checking is disabled")
	_, _, _, _, wm := setup()
	for c := 0; c < 256; c++ {
		func() {
			defer func() {
				if err := recover(); err == nil {
					t.Fail()
				}
			}()
			wm.Rank(wm.Len()+1, byte(c))
		}()
	}
}

func TestSelectBoundary(t *testing.T) {
	t.Skip("bounds checking is disabled")
	_, _, _, _, wm := setup()
	for c := 0; c < 256; c++ {
		func() {
			defer func() {
				if err := recover(); err == nil {
					t.Fail()
				}
			}()
			wm.Select(wm.RankLen(byte(c)), byte(c))
		}()
	}
}

func TestRankLessThanBoundary(t *testing.T) {
	t.Skip("bounds checking is disabled")
	_, _, _, _, wm := setup()
	for c := 0; c < 256; c++ {
		func() {
			defer func() {
				if err := recover(); err == nil {
					t.Fail()
				}
			}()
			wm.RankLessThan(wm.Len()+1, byte(c))
		}()
	}
}

func sameBitVector(a, b *bit.Vector) bool {
	abuf := bytes.NewBufferString("")
	a.Write(abuf)
	bbuf := bytes.NewBufferString("")
	b.Write(bbuf)
	return bytes.Equal(abuf.Bytes(), bbuf.Bytes())
}

func TestWriteRead(t *testing.T) {
	_, _, _, _, wm := setup()
	bf := bytes.NewBufferString("")
	if err := wm.Write(bf); err != nil {
		t.Error(err)
	}

	wmr, err := OpenMatrix(bytes.NewReader(bf.Bytes()))
	if err != nil {
		t.Error(err)
	}

	if wm.length != wmr.length {
		t.Fail()
	}
	for i, d := range wm.d {
		rd := wmr.d[i]
		if !sameBitVector(d.bv, rd.bv) {
			t.Fail()
		}
		if d.sep != rd.sep {
			t.Fail()
		}
	}
	for i := range wm.p {
		if wm.p[i] != wmr.p[i] {
			t.Fail()
		}
	}
}

// Copyright 2013 The Shellinford-Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package shellinford

import (
	"bitbucket.org/oov/go-shellinford/shellinford/bit"
	"bitbucket.org/oov/go-shellinford/shellinford/wavelet"
	"bufio"
	"bytes"
	"io/ioutil"
	"os"
	"sort"
	"testing"
)

func BenchmarkFMIndexBuild(b *testing.B) {
	filedata, err := ioutil.ReadFile("testdata/anime.txt")
	if err != nil {
		panic(err)
	}

	r := bytes.NewReader(filedata)
	sc := bufio.NewScanner(r)
	var fm *FMIndex

	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		b.StopTimer()
		r.Seek(0, os.SEEK_SET)
		sc = bufio.NewScanner(r)
		b.StartTimer()
		fm = NewFMIndex()
		for sc.Scan() {
			fm.Add(sc.Bytes())
		}
		fm.Build(0, 1)
	}
}

func BenchmarkFMIndex(b *testing.B) {
	f, err := os.Open("testdata/anime.txt")
	if err != nil {
		panic(err)
	}

	defer f.Close()
	sc := bufio.NewScanner(f)

	fm := NewFMIndex()
	for sc.Scan() {
		fm.Add(sc.Bytes())
		fm.Add(sc.Bytes())
	}

	fm.Build(0, 1)
}

type testPair struct {
	s []byte
	i int
}
type testPairSlice []testPair

func (p testPairSlice) Len() int           { return len(p) }
func (p testPairSlice) Less(i, j int) bool { return bytes.Compare(p[i].s, p[j].s) < 0 }
func (p testPairSlice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

func setup() (str []byte, rd map[string]int, pd []int, sd [][]byte, didd []int, docd [][]byte, fm *FMIndex) {
	fm = NewFMIndex()
	rd = make(map[string]int)

	did := 0
	endMarker := byte(1)
	docd = append(docd, []byte("abracadabra"))
	docd = append(docd, []byte("mississippi"))
	docd = append(docd, []byte("abracadabra mississippi"))
	for _, v := range docd {
		str = append(str, v...)
		for j := 0; j < len(v); j++ {
			didd = append(didd, did)
		}
		fm.Add(v)
		did++
	}
	didd = append(didd, did)
	str = append(str, endMarker)
	fm.Build(endMarker, 3)

	for i := 0; i < len(str); i++ {
		for j := 1; j <= len(str)-i; j++ {
			rd[string(str[i:i+j])]++
		}
	}

	var v testPairSlice
	for i := 0; i < len(str); i++ {
		s := string(str[i:]) + string(str[:i])
		v = append(v, testPair{[]byte(s), i})
	}
	sort.Sort(v)
	for _, vv := range v {
		pd = append(pd, vv.i)
	}

	for i := 0; i < len(str); i++ {
		sd = append(sd, str[i:])
	}
	return
}

func TestLen(t *testing.T) {
	str, _, _, _, _, _, fm := setup()
	if fm.Len() != len(str) {
		t.Logf("fm.Len() = %d / Success = %d", fm.Len(), len(str))
		t.Fail()
	}
}

func TestDocumentLen(t *testing.T) {
	_, _, _, _, _, docd, fm := setup()
	if fm.DocumentLen() != len(docd) {
		t.Logf("fm.DocumentLen() = %d / Success = %d", fm.DocumentLen(), len(docd))
		t.Fail()
	}
}

func TestRows(t *testing.T) {
	str, rd, _, _, _, _, fm := setup()
	for i := 0; i < fm.Len(); i++ {
		for j := i + 1; j < fm.Len(); j++ {
			s := str[i : i+j]
			n, _, _ := fm.Rows(s)
			if n != rd[string(s)] {
				t.Logf("fm.Rows(\"%s\") = %d, _, _ / Success = %d", string(s), n, rd[string(s)])
				t.Fail()
			}
		}
	}
}

func TestPos(t *testing.T) {
	_, _, pd, _, _, _, fm := setup()
	for i := 0; i < fm.Len(); i++ {
		if fm.Pos(i) != pd[i] {
			t.Logf("fm.Pos(%d) = %d / Success = %d", i, fm.Pos(i), pd[i])
			t.Fail()
		}
	}
}

func TestSubBytes(t *testing.T) {
	_, _, _, sd, _, _, fm := setup()
	for i := 0; i < fm.Len(); i++ {
		if !bytes.Equal(fm.SubBytes(i, fm.Len()), sd[i]) {
			t.Logf("fm.SubBytes(%d, %d) = %s / Success = %d", i, fm.Len(), string(fm.SubBytes(i, fm.Len())), sd[i])
			t.Fail()
		}
	}
}

func TestDocumentId(t *testing.T) {
	_, _, _, _, didd, _, fm := setup()
	for i := 0; i < fm.Len(); i++ {
		if fm.DocumentId(i) != didd[i] {
			t.Logf("fm.DocumentId(%d) = %d / Success = %d", i, fm.DocumentId(i), didd[i])
			t.Fail()
		}
	}
}

func TestDocument(t *testing.T) {
	_, _, _, _, _, docd, fm := setup()
	for i := 0; i < fm.DocumentLen(); i++ {
		if !bytes.Equal(fm.Document(i), docd[i]) {
			t.Logf("fm.Document(%d) = %s / Success = %s", i, string(fm.Document(i)), string(docd[i]))
			t.Fail()
		}
	}
}

func TestPosBoudary(t *testing.T) {
	_, _, _, _, _, _, fm := setup()
	defer func() {
		if recover() == nil {
			t.Fail()
		}
	}()
	fm.Pos(fm.Len())
}

func TestSubBytesBoudary(t *testing.T) {
	_, _, _, _, _, _, fm := setup()
	defer func() {
		if recover() == nil {
			t.Fail()
		}
	}()
	fm.SubBytes(fm.Len(), 0)
}

func TestDocumentIdBoudary(t *testing.T) {
	_, _, _, _, _, _, fm := setup()
	defer func() {
		if recover() == nil {
			t.Fail()
		}
	}()
	fm.DocumentId(fm.Len())
}

func TestDocumentBoudary(t *testing.T) {
	_, _, _, _, _, _, fm := setup()
	defer func() {
		if recover() == nil {
			t.Fail()
		}
	}()
	fm.Document(fm.DocumentLen())
}

func sameBitVector(a, b *bit.Vector) bool {
	abuf := bytes.NewBufferString("")
	a.Write(abuf)
	bbuf := bytes.NewBufferString("")
	b.Write(bbuf)
	return bytes.Equal(abuf.Bytes(), bbuf.Bytes())
}

func sameWaveletMatrix(a, b *wavelet.Matrix) bool {
	abuf := bytes.NewBufferString("")
	a.Write(abuf)
	bbuf := bytes.NewBufferString("")
	b.Write(bbuf)
	return bytes.Equal(abuf.Bytes(), bbuf.Bytes())
}

func TestWriteRead(t *testing.T) {
	_, _, _, _, _, _, fm := setup()
	bf := bytes.NewBufferString("")
	if err := fm.Write(bf); err != nil {
		t.Error(err)
	}

	fmr, err := OpenFMIndex(bytes.NewReader(bf.Bytes()))
	if err != nil {
		t.Error(err)
	}

	if fm.ddic != fmr.ddic {
		t.Fail()
	}

	if !sameBitVector(fm.doctails, fmr.doctails) {
		t.Fail()
	}

	if fm.head != fmr.head {
		t.Fail()
	}

	if len(fm.idic) != len(fmr.idic) {
		t.Fail()
	}
	for i, fmv := range fm.idic {
		if fmv != fmr.idic[i] {
			t.Fail()
		}
	}
	if len(fm.posdic) != len(fmr.posdic) {
		t.Fail()
	}
	for i, fmv := range fm.posdic {
		if fmv != fmr.posdic[i] {
			t.Fail()
		}
	}
	for i, fmv := range fm.rlt {
		if fmv != fmr.rlt[i] {
			t.Fail()
		}
	}
	if !sameWaveletMatrix(fm.sv, fmr.sv) {
		t.Fail()
	}
}

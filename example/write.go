package main

// $ go run write.go

import (
	"bitbucket.org/oov/go-shellinford/shellinford"
	"bufio"
	"fmt"
	"os"
	"time"
)

func main() {
	f, err := os.Open("anime.txt")
	if err != nil {
		panic(err)
	}

	defer f.Close()
	sc := bufio.NewScanner(f)

	fm := shellinford.NewFMIndex()
	st := time.Now()
	i := 0
	for sc.Scan() {
		fm.Add(sc.Bytes())
		i++
	}
	fmt.Printf("%d Entry added. (%fsecs)\n", i, time.Now().Sub(st).Seconds())

	fmt.Printf("Creating indexes...")
	st = time.Now()
	fm.Build(0, 1)
	fmt.Printf("done. (%fsecs)\n", time.Now().Sub(st).Seconds())

	of, err := os.Create("index")
	if err != nil {
		panic(err)
	}
	defer of.Close()

	st = time.Now()
	err = fm.Write(of)
	if err != nil {
		panic(err)
	}
	fmt.Printf("index file saved. (%fsecs)\n", time.Now().Sub(st).Seconds())
}
